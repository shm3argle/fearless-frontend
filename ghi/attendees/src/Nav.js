import React from 'react';

function Nav() {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">Conference GO!</a>


              <li>
                <a>Home</a>
              </li>
              <li>
                <a>New location</a>
              </li>
              <li>
                <a>New conference</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" aria-current="page" href="new-location.html">New presentation</a>

              </li>


        </div>
      </nav>
    </header>
  );
}

export default Nav;
